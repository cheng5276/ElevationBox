﻿namespace WinForm
{
    partial class WinMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinMain));
            this.panel = new System.Windows.Forms.Panel();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出高程点ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出TINToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出等高线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出拓扑数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.基本操作ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入高程点ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成不规则三角网ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成等高线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.拓展功能ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.计算土方量ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示拓扑数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助文档ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于软件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.contours_check = new System.Windows.Forms.RadioButton();
            this.lines_check = new System.Windows.Forms.RadioButton();
            this.points_check = new System.Windows.Forms.RadioButton();
            this.导入高程点 = new System.Windows.Forms.Button();
            this.生成不规则三角网 = new System.Windows.Forms.Button();
            this.生成等高线 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.计算土方量 = new System.Windows.Forms.Button();
            this.显示拓扑数据 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.kilometer = new System.Windows.Forms.RadioButton();
            this.millimeter = new System.Windows.Forms.RadioButton();
            this.centimeter = new System.Windows.Forms.RadioButton();
            this.meter = new System.Windows.Forms.RadioButton();
            this.MenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.Control;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.Location = new System.Drawing.Point(149, 25);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(835, 616);
            this.panel.TabIndex = 0;
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.基本操作ToolStripMenuItem,
            this.拓展功能ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(984, 25);
            this.MenuStrip.TabIndex = 1;
            this.MenuStrip.Text = "MenuStrip";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导出ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 导出ToolStripMenuItem
            // 
            this.导出ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导出高程点ToolStripMenuItem,
            this.导出TINToolStripMenuItem,
            this.导出等高线ToolStripMenuItem,
            this.导出拓扑数据ToolStripMenuItem});
            this.导出ToolStripMenuItem.Name = "导出ToolStripMenuItem";
            this.导出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.导出ToolStripMenuItem.Text = "导出";
            // 
            // 导出高程点ToolStripMenuItem
            // 
            this.导出高程点ToolStripMenuItem.Name = "导出高程点ToolStripMenuItem";
            this.导出高程点ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导出高程点ToolStripMenuItem.Text = "高程点";
            this.导出高程点ToolStripMenuItem.Click += new System.EventHandler(this.导出高程点ToolStripMenuItem_Click);
            // 
            // 导出TINToolStripMenuItem
            // 
            this.导出TINToolStripMenuItem.Name = "导出TINToolStripMenuItem";
            this.导出TINToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导出TINToolStripMenuItem.Text = "TIN";
            this.导出TINToolStripMenuItem.Click += new System.EventHandler(this.导出TINToolStripMenuItem_Click);
            // 
            // 导出等高线ToolStripMenuItem
            // 
            this.导出等高线ToolStripMenuItem.Name = "导出等高线ToolStripMenuItem";
            this.导出等高线ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导出等高线ToolStripMenuItem.Text = "等高线";
            this.导出等高线ToolStripMenuItem.Click += new System.EventHandler(this.导出等高线ToolStripMenuItem_Click);
            // 
            // 导出拓扑数据ToolStripMenuItem
            // 
            this.导出拓扑数据ToolStripMenuItem.Name = "导出拓扑数据ToolStripMenuItem";
            this.导出拓扑数据ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导出拓扑数据ToolStripMenuItem.Text = "拓扑数据";
            this.导出拓扑数据ToolStripMenuItem.Click += new System.EventHandler(this.导出拓扑数据ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 基本操作ToolStripMenuItem
            // 
            this.基本操作ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入高程点ToolStripMenuItem,
            this.生成不规则三角网ToolStripMenuItem,
            this.生成等高线ToolStripMenuItem});
            this.基本操作ToolStripMenuItem.Name = "基本操作ToolStripMenuItem";
            this.基本操作ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.基本操作ToolStripMenuItem.Text = "基本操作";
            // 
            // 导入高程点ToolStripMenuItem
            // 
            this.导入高程点ToolStripMenuItem.Name = "导入高程点ToolStripMenuItem";
            this.导入高程点ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.导入高程点ToolStripMenuItem.Text = "导入高程点";
            this.导入高程点ToolStripMenuItem.Click += new System.EventHandler(this.导入高程点_Click);
            // 
            // 生成不规则三角网ToolStripMenuItem
            // 
            this.生成不规则三角网ToolStripMenuItem.Name = "生成不规则三角网ToolStripMenuItem";
            this.生成不规则三角网ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.生成不规则三角网ToolStripMenuItem.Text = "生成不规则三角网";
            this.生成不规则三角网ToolStripMenuItem.Click += new System.EventHandler(this.生成不规则三角网_Click);
            // 
            // 生成等高线ToolStripMenuItem
            // 
            this.生成等高线ToolStripMenuItem.Name = "生成等高线ToolStripMenuItem";
            this.生成等高线ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.生成等高线ToolStripMenuItem.Text = "生成等高线";
            this.生成等高线ToolStripMenuItem.Click += new System.EventHandler(this.生成等高线_Click);
            // 
            // 拓展功能ToolStripMenuItem
            // 
            this.拓展功能ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.计算土方量ToolStripMenuItem,
            this.显示拓扑数据ToolStripMenuItem});
            this.拓展功能ToolStripMenuItem.Name = "拓展功能ToolStripMenuItem";
            this.拓展功能ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.拓展功能ToolStripMenuItem.Text = "拓展功能";
            // 
            // 计算土方量ToolStripMenuItem
            // 
            this.计算土方量ToolStripMenuItem.Name = "计算土方量ToolStripMenuItem";
            this.计算土方量ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.计算土方量ToolStripMenuItem.Text = "计算土方量";
            this.计算土方量ToolStripMenuItem.Click += new System.EventHandler(this.计算土方量_Click);
            // 
            // 显示拓扑数据ToolStripMenuItem
            // 
            this.显示拓扑数据ToolStripMenuItem.Name = "显示拓扑数据ToolStripMenuItem";
            this.显示拓扑数据ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.显示拓扑数据ToolStripMenuItem.Text = "显示拓扑数据";
            this.显示拓扑数据ToolStripMenuItem.Click += new System.EventHandler(this.显示拓扑数据_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.帮助文档ToolStripMenuItem,
            this.关于软件ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 帮助文档ToolStripMenuItem
            // 
            this.帮助文档ToolStripMenuItem.Name = "帮助文档ToolStripMenuItem";
            this.帮助文档ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.帮助文档ToolStripMenuItem.Text = "帮助文档";
            this.帮助文档ToolStripMenuItem.Click += new System.EventHandler(this.帮助文档ToolStripMenuItem_Click);
            // 
            // 关于软件ToolStripMenuItem
            // 
            this.关于软件ToolStripMenuItem.Name = "关于软件ToolStripMenuItem";
            this.关于软件ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关于软件ToolStripMenuItem.Text = "关于软件";
            this.关于软件ToolStripMenuItem.Click += new System.EventHandler(this.关于软件ToolStripMenuItem_Click);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.contours_check);
            this.groupBox1.Controls.Add(this.lines_check);
            this.groupBox1.Controls.Add(this.points_check);
            this.groupBox1.Location = new System.Drawing.Point(10, 304);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(133, 105);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "图层管理";
            // 
            // contours_check
            // 
            this.contours_check.AutoSize = true;
            this.contours_check.Location = new System.Drawing.Point(17, 77);
            this.contours_check.Name = "contours_check";
            this.contours_check.Size = new System.Drawing.Size(59, 16);
            this.contours_check.TabIndex = 2;
            this.contours_check.Text = "等高线";
            this.contours_check.UseVisualStyleBackColor = true;
            this.contours_check.CheckedChanged += new System.EventHandler(this.contours_check_CheckedChanged);
            // 
            // lines_check
            // 
            this.lines_check.AutoSize = true;
            this.lines_check.Location = new System.Drawing.Point(17, 50);
            this.lines_check.Name = "lines_check";
            this.lines_check.Size = new System.Drawing.Size(41, 16);
            this.lines_check.TabIndex = 1;
            this.lines_check.Text = "TIN";
            this.lines_check.UseVisualStyleBackColor = true;
            this.lines_check.CheckedChanged += new System.EventHandler(this.lines_check_CheckedChanged);
            // 
            // points_check
            // 
            this.points_check.AutoSize = true;
            this.points_check.Checked = true;
            this.points_check.Location = new System.Drawing.Point(17, 25);
            this.points_check.Name = "points_check";
            this.points_check.Size = new System.Drawing.Size(59, 16);
            this.points_check.TabIndex = 0;
            this.points_check.TabStop = true;
            this.points_check.Text = "高程点";
            this.points_check.UseVisualStyleBackColor = true;
            this.points_check.CheckedChanged += new System.EventHandler(this.points_check_CheckedChanged);
            // 
            // 导入高程点
            // 
            this.导入高程点.Location = new System.Drawing.Point(15, 22);
            this.导入高程点.Name = "导入高程点";
            this.导入高程点.Size = new System.Drawing.Size(102, 30);
            this.导入高程点.TabIndex = 0;
            this.导入高程点.Text = "导入高程点";
            this.导入高程点.UseVisualStyleBackColor = true;
            this.导入高程点.Click += new System.EventHandler(this.导入高程点_Click);
            // 
            // 生成不规则三角网
            // 
            this.生成不规则三角网.Location = new System.Drawing.Point(15, 59);
            this.生成不规则三角网.Name = "生成不规则三角网";
            this.生成不规则三角网.Size = new System.Drawing.Size(102, 30);
            this.生成不规则三角网.TabIndex = 1;
            this.生成不规则三角网.Text = "生成TIN";
            this.生成不规则三角网.UseVisualStyleBackColor = true;
            this.生成不规则三角网.Click += new System.EventHandler(this.生成不规则三角网_Click);
            // 
            // 生成等高线
            // 
            this.生成等高线.Location = new System.Drawing.Point(15, 96);
            this.生成等高线.Name = "生成等高线";
            this.生成等高线.Size = new System.Drawing.Size(102, 30);
            this.生成等高线.TabIndex = 2;
            this.生成等高线.Text = "生成等高线";
            this.生成等高线.UseVisualStyleBackColor = true;
            this.生成等高线.Click += new System.EventHandler(this.生成等高线_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.生成等高线);
            this.groupBox2.Controls.Add(this.生成不规则三角网);
            this.groupBox2.Controls.Add(this.导入高程点);
            this.groupBox2.Location = new System.Drawing.Point(10, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(133, 138);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基本操作";
            // 
            // 计算土方量
            // 
            this.计算土方量.Location = new System.Drawing.Point(15, 22);
            this.计算土方量.Name = "计算土方量";
            this.计算土方量.Size = new System.Drawing.Size(102, 30);
            this.计算土方量.TabIndex = 1;
            this.计算土方量.Text = "计算土方量";
            this.计算土方量.UseVisualStyleBackColor = true;
            this.计算土方量.Click += new System.EventHandler(this.计算土方量_Click);
            // 
            // 显示拓扑数据
            // 
            this.显示拓扑数据.Location = new System.Drawing.Point(15, 63);
            this.显示拓扑数据.Name = "显示拓扑数据";
            this.显示拓扑数据.Size = new System.Drawing.Size(102, 30);
            this.显示拓扑数据.TabIndex = 2;
            this.显示拓扑数据.Text = "显示拓朴数据";
            this.显示拓扑数据.UseVisualStyleBackColor = true;
            this.显示拓扑数据.Click += new System.EventHandler(this.显示拓扑数据_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.显示拓扑数据);
            this.groupBox3.Controls.Add(this.计算土方量);
            this.groupBox3.Location = new System.Drawing.Point(10, 186);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(133, 105);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "拓展功能";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pictureBox3);
            this.groupBox4.Controls.Add(this.pictureBox2);
            this.groupBox4.Controls.Add(this.pictureBox1);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(10, 510);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(133, 111);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "设置画笔颜色";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.LimeGreen;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(66, 78);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(42, 20);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.BackColorChanged += new System.EventHandler(this.pictureBox3_BackColorChanged);
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DodgerBlue;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Location = new System.Drawing.Point(66, 52);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(42, 20);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.BackColorChanged += new System.EventHandler(this.pictureBox2_BackColorChanged);
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Salmon;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(66, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 20);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.BackColorChanged += new System.EventHandler(this.pictureBox1_BackColorChanged);
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "等高线：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "TIN：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "高程点：";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.kilometer);
            this.groupBox5.Controls.Add(this.millimeter);
            this.groupBox5.Controls.Add(this.centimeter);
            this.groupBox5.Controls.Add(this.meter);
            this.groupBox5.Location = new System.Drawing.Point(10, 422);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 75);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "设置显示单位";
            // 
            // kilometer
            // 
            this.kilometer.AutoSize = true;
            this.kilometer.Location = new System.Drawing.Point(15, 47);
            this.kilometer.Name = "kilometer";
            this.kilometer.Size = new System.Drawing.Size(47, 16);
            this.kilometer.TabIndex = 2;
            this.kilometer.Text = "千米";
            this.kilometer.UseVisualStyleBackColor = true;
            this.kilometer.CheckedChanged += new System.EventHandler(this.kilometer_CheckedChanged);
            // 
            // millimeter
            // 
            this.millimeter.AutoSize = true;
            this.millimeter.Location = new System.Drawing.Point(66, 47);
            this.millimeter.Name = "millimeter";
            this.millimeter.Size = new System.Drawing.Size(47, 16);
            this.millimeter.TabIndex = 3;
            this.millimeter.Text = "毫米";
            this.millimeter.UseVisualStyleBackColor = true;
            this.millimeter.CheckedChanged += new System.EventHandler(this.millimeter_CheckedChanged);
            // 
            // centimeter
            // 
            this.centimeter.AutoSize = true;
            this.centimeter.Location = new System.Drawing.Point(66, 21);
            this.centimeter.Name = "centimeter";
            this.centimeter.Size = new System.Drawing.Size(47, 16);
            this.centimeter.TabIndex = 1;
            this.centimeter.Text = "厘米";
            this.centimeter.UseVisualStyleBackColor = true;
            this.centimeter.CheckedChanged += new System.EventHandler(this.centimeter_CheckedChanged);
            // 
            // meter
            // 
            this.meter.AutoSize = true;
            this.meter.Checked = true;
            this.meter.Location = new System.Drawing.Point(15, 21);
            this.meter.Name = "meter";
            this.meter.Size = new System.Drawing.Size(35, 16);
            this.meter.TabIndex = 0;
            this.meter.TabStop = true;
            this.meter.Text = "米";
            this.meter.UseVisualStyleBackColor = true;
            this.meter.CheckedChanged += new System.EventHandler(this.meter_CheckedChanged);
            // 
            // WinMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(984, 641);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.MenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WinMain";
            this.Opacity = 0.2D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ElevationBox";
            this.Load += new System.EventHandler(this.WinMain_Load);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 基本操作ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入高程点ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成不规则三角网ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成等高线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助文档ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于软件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 拓展功能ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 计算土方量ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示拓扑数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button 导入高程点;
        private System.Windows.Forms.Button 生成不规则三角网;
        private System.Windows.Forms.Button 生成等高线;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button 计算土方量;
        private System.Windows.Forms.Button 显示拓扑数据;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton contours_check;
        private System.Windows.Forms.RadioButton lines_check;
        private System.Windows.Forms.RadioButton points_check;
        private System.Windows.Forms.ToolStripMenuItem 导出高程点ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出TINToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripMenuItem 导出等高线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出拓扑数据ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton millimeter;
        private System.Windows.Forms.RadioButton kilometer;
        private System.Windows.Forms.RadioButton centimeter;
        private System.Windows.Forms.RadioButton meter;
    }
}

