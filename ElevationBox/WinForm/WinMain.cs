﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

namespace WinForm
{
    public partial class WinMain : Form
    {
        public WinMain()
        {
            InitializeComponent();
        }
        
        private void timer_Tick(object sender, EventArgs e)
        {
            this.Opacity += 0.05;
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private TIN tin = new TIN();
        private List<Bitmap> bmp = new List<Bitmap>();
        private string[] unit = new string[2];

        private void 导入高程点_Click(object sender, EventArgs e)
        {
            ImportPoint form2 = new ImportPoint();
            form2.ShowDialog();
            //若返回值为“OK”代表为自动导入
            if (form2.DialogResult == DialogResult.OK)
            {
                //清空原有的数据
                tin.Clear();
                bmp.Clear();
                //写入新的数据
                StreamReader sr = new StreamReader(form2.filepath.Text, Encoding.UTF8);
                string data = sr.ReadLine();
                while (data != null && data != "")
                {
                    if(tin.input_points(data)==false)
                    {
                        MessageBox.Show("数据格式存在错误，请检查数据！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tin.Clear();
                        sr.Close();
                        form2.Close();
                        return;
                    }
                    data = sr.ReadLine();
                }
                sr.Close();
                form2.Close();
                if (tin.Count_Point() < 3)
                {
                    MessageBox.Show("导入的高程点数量少于3个！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tin.Clear();
                    bmp.Clear();
                    return;
                }
                //显示高程点数据
                bmp.Add(tin.Draw_Points(panel.Width, panel.Height, pictureBox1.BackColor));
                points_check.Checked = true;
                lines_check.Checked=false;
                contours_check.Checked = false;
                panel.BackgroundImage = bmp[0];
                return;
            }
            //若返回值为“YES”代表为手动输入
            if (form2.DialogResult == DialogResult.Yes)
            {
                //清空原有的数据
                tin.Clear();
                bmp.Clear();
                //写入新的数据
                for (int i = 0; i < form2.stringdata.Lines.Length; i++)
                {
                    string data = form2.stringdata.Lines[i];                    
                    if (data != "")
                    {
                        if(tin.input_points(data)==false)
                        {
                            MessageBox.Show("数据格式存在错误，请检查数据！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tin.Clear();
                            form2.Close();
                            return;
                        }
                    }
                    else
                    {
                        form2.Close();
                        break;
                    }
                }
                if (tin.Count_Point() < 3)
                {
                    MessageBox.Show("导入的高程点数量少于3个！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tin.Clear();
                    bmp.Clear();
                    return;
                }
                //显示高程点数据
                bmp.Add(tin.Draw_Points(panel.Width, panel.Height, pictureBox1.BackColor));
                points_check.Checked = true;
                lines_check.Checked = false;
                contours_check.Checked = false;
                panel.BackgroundImage = bmp[0];
                return;
            }
        }

        private void 生成不规则三角网_Click(object sender, EventArgs e)
        {
            if (tin.Count_Point() == 0)
            {
                MessageBox.Show("请先“导入高程点”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Loading loadingfrm = new Loading(this);
            //将Loaing窗口，注入到 SplashScreenManager 来管理
            SplashScreenManager loading = new SplashScreenManager(loadingfrm);
            loading.ShowLoading();
            tin.create_tin();
            loading.CloseWaitForm();
            //显示三角网数据
            Bitmap temp = tin.Draw_Lines(panel.Width, panel.Height, pictureBox2.BackColor);            
            if (bmp.Count > 1)
            {
                bmp[1] = temp;
            }
            else
            {
                bmp.Add(temp);
            }
            points_check.Checked = false;
            lines_check.Checked = true;
            contours_check.Checked = false;
            panel.BackgroundImage = bmp[1];
        }

        private void 生成等高线_Click(object sender, EventArgs e)
        {
            if (tin.Count_Tri() == 0)
            {
                MessageBox.Show("请先“生成TIN”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            ImportDistance form2 = new ImportDistance();
            form2.lengthunit.Text = unit[0];
            form2.ShowDialog();
            double distance = 0;
            if (form2.DialogResult == DialogResult.OK)
            {
                distance = Convert.ToDouble(form2.stringdata.Text);
                form2.Close();
            }
            else
            {
                form2.Close();             
                return;
            }
            tin.Clear_Contour();
            //生成等高线
            Loading loadingfrm = new Loading(this);
            //将Loaing窗口，注入到 SplashScreenManager 来管理
            SplashScreenManager loading = new SplashScreenManager(loadingfrm);
            loading.ShowLoading();
            tin.create_contour(distance);
            loading.CloseWaitForm();
            if (tin.Count_Contour() == 0)
            {
                MessageBox.Show("生成等高线的数量为0！\n原因：输入的等高距过大，已超出数据点集的最大高程与最小高程之差！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //显示等高线
            Bitmap temp = tin.Draw_Contour(panel.Width, panel.Height, pictureBox3.BackColor);
            if (bmp.Count > 2)
            {
                bmp[2] = temp;
            }
            else
            {
                bmp.Add(temp);
            }
            points_check.Checked = false;
            lines_check.Checked = false;
            contours_check.Checked = true;
            panel.BackgroundImage = bmp[2];
        }

        private void 计算土方量_Click(object sender, EventArgs e)
        {
            if (tin.Count_Tri() == 0)
            {
                MessageBox.Show("请先“生成TIN”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            ImportHeight form2 = new ImportHeight(tin);
            form2.unit1.Text = unit[0];
            form2.unit2.Text = form2.unit3.Text = unit[1];
            form2.ShowDialog();
        }

        private void 显示拓扑数据_Click(object sender, EventArgs e)
        {
            if (tin.Count_Tri() == 0)
            {
                MessageBox.Show("请先“生成TIN”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DisplayTopology form2 = new DisplayTopology(tin);
            form2.ShowDialog();
        }  

        private void 导出TINToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tin.Count_Tri() == 0)
            {
                MessageBox.Show("请先“生成TIN”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "DXF格式|*.dxf";
            sfd.FileName = "TIN.dxf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                tin.Export_TIN_DXF(sfd.FileName);
            }
        }

        private void 导出高程点ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tin.Count_Point() == 0)
            {
                MessageBox.Show("请先“导入高程点”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "DXF格式|*.dxf";
            sfd.FileName = "Point.dxf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                tin.Export_Point_DXF(sfd.FileName);
            }
        }

        private void 导出等高线ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tin.Count_Contour() == 0)
            {
                MessageBox.Show("请先“生成等高线”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "DXF格式|*.dxf";
            sfd.FileName = "等高线.dxf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                tin.Export_Contour_DXF(sfd.FileName);
            }
        }

        private void 导出拓扑数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tin.Count_Tri() == 0)
            {
                MessageBox.Show("请先“生成TIN”！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "选择文件夹";            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.SelectedPath;
                tin.Export_Topology(path);
            }
        }

        //切换为显示高程点
        private void points_check_CheckedChanged(object sender, EventArgs e)
        {
            if(points_check.Checked == true)
            {
                if(bmp.Count > 0)
                {
                    panel.BackgroundImage = bmp[0];
                }
                else panel.BackgroundImage = new Bitmap(panel.Width, panel.Height);
            }
        }

        //切换为显示TIN
        private void lines_check_CheckedChanged(object sender, EventArgs e)
        {
            if (lines_check.Checked == true)
            {
                if(bmp.Count > 1)
                {
                     panel.BackgroundImage = bmp[1];
                }
               else panel.BackgroundImage = new Bitmap(panel.Width, panel.Height);
            }            
        }
        
        //切换为显示等高线
        private void contours_check_CheckedChanged(object sender, EventArgs e)
        {
            if (contours_check.Checked == true)
            {
                if (bmp.Count > 2)
                {
                    panel.BackgroundImage = bmp[2];
                }
                else panel.BackgroundImage = new Bitmap(panel.Width, panel.Height);
            }
        }

        //设置高程点画笔颜色
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                pictureBox1.BackColor = this.colorDialog.Color;
            }
        }

        //设置TIN画笔颜色
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                pictureBox2.BackColor = this.colorDialog.Color;
            }
        }

        //设置等高线画笔颜色
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                pictureBox3.BackColor = this.colorDialog.Color;
            }
        }

        private void WinMain_Load(object sender, EventArgs e)
        {
            panel.BackgroundImage = new Bitmap(panel.Width, panel.Height);
            unit[0] = "米";
            unit[1] = "立方米";
        }

        private void 关于软件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void 帮助文档ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(System.Environment.CurrentDirectory + "\\Help.chm"))
            {
                System.Diagnostics.Process.Start(System.Environment.CurrentDirectory + "\\Help.chm");
            }
            else
            {
                MessageBox.Show("程序根目录中，文件[Help.chm]不存在，该文件已被删除或者移动！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 
        }

        private void pictureBox1_BackColorChanged(object sender, EventArgs e)
        {
            if (bmp.Count > 0)
            {
                bmp[0] = tin.Draw_Points(panel.Width, panel.Height, pictureBox1.BackColor);
                if (points_check.Checked == true)
                {
                    panel.BackgroundImage = bmp[0];
                }
            }
        }

        private void pictureBox2_BackColorChanged(object sender, EventArgs e)
        {
            if (bmp.Count > 1)
            {
                bmp[1] = tin.Draw_Lines(panel.Width, panel.Height, pictureBox2.BackColor);
                if (lines_check.Checked == true)
                {
                    panel.BackgroundImage = bmp[1];
                }
            }
        }

        private void pictureBox3_BackColorChanged(object sender, EventArgs e)
        {
            if (bmp.Count > 2)
            {
                bmp[2] = tin.Draw_Contour(panel.Width, panel.Height, pictureBox3.BackColor);
                if (contours_check.Checked == true)
                {
                    panel.BackgroundImage = bmp[2];
                }
            }
        }

        //切换显示单位为米
        private void meter_CheckedChanged(object sender, EventArgs e)
        {
            if (meter.Checked == true)
            {
                unit[0] = "米";
                unit[1] = "立方米";
            }
        }

        //切换显示单位为厘米
        private void centimeter_CheckedChanged(object sender, EventArgs e)
        {
            if (centimeter.Checked == true)
            {
                unit[0] = "厘米";
                unit[1] = "立方厘米";
            }
        }

        //切换显示单位为千米
        private void kilometer_CheckedChanged(object sender, EventArgs e)
        {
            if (kilometer.Checked == true)
            {
                unit[0] = "千米";
                unit[1] = "立方千米";
            }
        }

        //切换显示单位为毫米
        private void millimeter_CheckedChanged(object sender, EventArgs e)
        {
            if (millimeter.Checked == true)
            {
                unit[0] = "毫米";
                unit[1] = "立方毫米";
            }
        } 
    }
}