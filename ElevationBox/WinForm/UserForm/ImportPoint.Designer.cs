﻿namespace WinForm
{
    partial class ImportPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportPoint));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.getpoint1 = new System.Windows.Forms.Button();
            this.choosefile = new System.Windows.Forms.Button();
            this.filepath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.getpoint2 = new System.Windows.Forms.Button();
            this.cleardata = new System.Windows.Forms.Button();
            this.stringdata = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.getpoint1);
            this.groupBox1.Controls.Add(this.choosefile);
            this.groupBox1.Controls.Add(this.filepath);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "自动导入";
            // 
            // getpoint1
            // 
            this.getpoint1.Location = new System.Drawing.Point(142, 75);
            this.getpoint1.Name = "getpoint1";
            this.getpoint1.Size = new System.Drawing.Size(75, 30);
            this.getpoint1.TabIndex = 3;
            this.getpoint1.Text = "导入";
            this.getpoint1.UseVisualStyleBackColor = true;
            this.getpoint1.Click += new System.EventHandler(this.getpoint1_Click);
            // 
            // choosefile
            // 
            this.choosefile.Location = new System.Drawing.Point(44, 75);
            this.choosefile.Name = "choosefile";
            this.choosefile.Size = new System.Drawing.Size(75, 30);
            this.choosefile.TabIndex = 2;
            this.choosefile.Text = "选择文件";
            this.choosefile.UseVisualStyleBackColor = true;
            this.choosefile.Click += new System.EventHandler(this.choosefile_Click);
            // 
            // filepath
            // 
            this.filepath.Enabled = false;
            this.filepath.Location = new System.Drawing.Point(19, 43);
            this.filepath.Name = "filepath";
            this.filepath.Size = new System.Drawing.Size(223, 21);
            this.filepath.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "高程点文件路径:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.getpoint2);
            this.groupBox2.Controls.Add(this.cleardata);
            this.groupBox2.Controls.Add(this.stringdata);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 139);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 210);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "手动输入";
            // 
            // getpoint2
            // 
            this.getpoint2.Location = new System.Drawing.Point(142, 167);
            this.getpoint2.Name = "getpoint2";
            this.getpoint2.Size = new System.Drawing.Size(75, 30);
            this.getpoint2.TabIndex = 4;
            this.getpoint2.Text = "导入";
            this.getpoint2.UseVisualStyleBackColor = true;
            this.getpoint2.Click += new System.EventHandler(this.getpoint2_Click);
            // 
            // cleardata
            // 
            this.cleardata.Location = new System.Drawing.Point(44, 167);
            this.cleardata.Name = "cleardata";
            this.cleardata.Size = new System.Drawing.Size(75, 30);
            this.cleardata.TabIndex = 2;
            this.cleardata.Text = "清空";
            this.cleardata.UseVisualStyleBackColor = true;
            this.cleardata.Click += new System.EventHandler(this.cleardata_Click);
            // 
            // stringdata
            // 
            this.stringdata.Location = new System.Drawing.Point(19, 42);
            this.stringdata.MaxLength = 0;
            this.stringdata.Multiline = true;
            this.stringdata.Name = "stringdata";
            this.stringdata.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.stringdata.Size = new System.Drawing.Size(223, 119);
            this.stringdata.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "高程点数据：(格式：X,Y,Z)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "- - - - - - - - - - - - - - - - - - - - - - - -\r\n";
            // 
            // ImportPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 361);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportPoint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "导入高程点";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button getpoint1;
        private System.Windows.Forms.Button choosefile;
        private System.Windows.Forms.Button getpoint2;
        private System.Windows.Forms.Button cleardata;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox filepath;
        public System.Windows.Forms.TextBox stringdata;
    }
}