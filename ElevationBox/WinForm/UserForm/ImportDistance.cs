﻿using System;
using System.Windows.Forms;

namespace WinForm
{
    public partial class ImportDistance : Form
    {
        public ImportDistance()
        {
            InitializeComponent();
        }

        private void cleardata_Click(object sender, EventArgs e)
        {
            stringdata.Text = "";
        }

        private void getdistance_Click(object sender, EventArgs e)
        {
            if (stringdata.Text == "")
            {
                MessageBox.Show("请输入等高距！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                double data = Convert.ToDouble(stringdata.Text);
            }
            catch
            {
                MessageBox.Show("输入的数据格式有误，请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (Convert.ToDouble(stringdata.Text) <= 0)
            {
                MessageBox.Show("输入的数据格式有误，请重新一个大于0的正数！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return; 
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}
