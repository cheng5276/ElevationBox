﻿namespace WinForm
{
    partial class ImportDistance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportDistance));
            this.label1 = new System.Windows.Forms.Label();
            this.stringdata = new System.Windows.Forms.TextBox();
            this.getdistance = new System.Windows.Forms.Button();
            this.cleardata = new System.Windows.Forms.Button();
            this.lengthunit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "等高距：";
            // 
            // stringdata
            // 
            this.stringdata.Location = new System.Drawing.Point(80, 21);
            this.stringdata.Name = "stringdata";
            this.stringdata.Size = new System.Drawing.Size(149, 21);
            this.stringdata.TabIndex = 1;
            // 
            // getdistance
            // 
            this.getdistance.Location = new System.Drawing.Point(154, 60);
            this.getdistance.Name = "getdistance";
            this.getdistance.Size = new System.Drawing.Size(75, 30);
            this.getdistance.TabIndex = 6;
            this.getdistance.Text = "确定";
            this.getdistance.UseVisualStyleBackColor = true;
            this.getdistance.Click += new System.EventHandler(this.getdistance_Click);
            // 
            // cleardata
            // 
            this.cleardata.Location = new System.Drawing.Point(56, 60);
            this.cleardata.Name = "cleardata";
            this.cleardata.Size = new System.Drawing.Size(75, 30);
            this.cleardata.TabIndex = 5;
            this.cleardata.Text = "清空";
            this.cleardata.UseVisualStyleBackColor = true;
            this.cleardata.Click += new System.EventHandler(this.cleardata_Click);
            // 
            // lengthunit
            // 
            this.lengthunit.AutoSize = true;
            this.lengthunit.Location = new System.Drawing.Point(234, 26);
            this.lengthunit.Name = "lengthunit";
            this.lengthunit.Size = new System.Drawing.Size(11, 12);
            this.lengthunit.TabIndex = 7;
            this.lengthunit.Text = " ";
            // 
            // ImportDistance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 111);
            this.Controls.Add(this.lengthunit);
            this.Controls.Add(this.getdistance);
            this.Controls.Add(this.cleardata);
            this.Controls.Add(this.stringdata);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportDistance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "输入等高距";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getdistance;
        private System.Windows.Forms.Button cleardata;
        public System.Windows.Forms.TextBox stringdata;
        public System.Windows.Forms.Label lengthunit;
    }
}