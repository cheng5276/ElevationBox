﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace WinForm
{
    public partial class DisplayTopology : Form
    {
        private TIN tin;

        public DisplayTopology(TIN tin)
        {
            InitializeComponent();
            this.tin = tin;
        }
        
        //显示点
        private void Display_Point()
        {
            dataGridView1.ReadOnly = false;
            while (dataGridView1.ColumnCount != 0)
            {
                dataGridView1.Columns.RemoveAt(0);
            }
            TIN_Point[] point = tin.Get_Point();
            for (int i = 0; i < 4; i++)
            {
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            dataGridView1.Columns[0].HeaderText = "序号";
            dataGridView1.Columns[1].HeaderText = "X坐标";
            dataGridView1.Columns[2].HeaderText = "Y坐标";
            dataGridView1.Columns[3].HeaderText = "Z坐标";
            for (int i = 0; i < point.Count(); i++)
            {
                int index = this.dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = i;
                dataGridView1.Rows[index].Cells[1].Value = point[i].X;
                dataGridView1.Rows[index].Cells[2].Value = point[i].Y;
                dataGridView1.Rows[index].Cells[3].Value = point[i].Z;
            }
            dataGridView1.ReadOnly = true;
        }

        //显示线段
        private void Display_Line()
        {
            dataGridView1.ReadOnly = false;
            while (dataGridView1.ColumnCount != 0)
            {
                dataGridView1.Columns.RemoveAt(0);
            }
            TIN_Line[] line = tin.Get_Line();
            for (int i = 0; i < 3; i++)
            {
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            dataGridView1.Columns[0].HeaderText = "序号";
            dataGridView1.Columns[1].HeaderText = "起始点标号";
            dataGridView1.Columns[2].HeaderText = "终止点标号";
            for (int i = 0; i < line.Count(); i++)
            {
                int index = this.dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = i;
                dataGridView1.Rows[index].Cells[1].Value = line[i].P0;
                dataGridView1.Rows[index].Cells[2].Value = line[i].P1;
            }
            dataGridView1.ReadOnly = true;
        }

        //显示三角形
        private void Display_Tri()
        {
            dataGridView1.ReadOnly = false;
            while (dataGridView1.ColumnCount != 0)
            {
                dataGridView1.Columns.RemoveAt(0);
            }
            TIN_Tri[] tri = tin.Get_Tri();
            for (int i = 0; i < 7; i++)
            {
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            dataGridView1.Columns[0].HeaderText = "序号";
            dataGridView1.Columns[1].HeaderText = "线段1标号";
            dataGridView1.Columns[2].HeaderText = "线段2标号";
            dataGridView1.Columns[3].HeaderText = "线段3标号";
            dataGridView1.Columns[4].HeaderText = "线段1邻接三角形";
            dataGridView1.Columns[5].HeaderText = "线段2邻接三角形";
            dataGridView1.Columns[6].HeaderText = "线段3邻接三角形";
            for (int i = 0; i < tri.Count(); i++)
            {
                int index = this.dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = i;
                dataGridView1.Rows[index].Cells[1].Value = tri[i].L0;
                dataGridView1.Rows[index].Cells[2].Value = tri[i].L1;
                dataGridView1.Rows[index].Cells[3].Value = tri[i].L2;
                dataGridView1.Rows[index].Cells[4].Value = tri[i].Adjoin[0];
                dataGridView1.Rows[index].Cells[5].Value = tri[i].Adjoin[1];
                dataGridView1.Rows[index].Cells[6].Value = tri[i].Adjoin[2];
            }
            dataGridView1.ReadOnly = true;        
        }

        //切换为显示点
        private void points_check_CheckedChanged(object sender, EventArgs e)
        {
            if (points_check.Checked == true)
            {
                Loading loadingfrm = new Loading(this);
                SplashScreenManager loading = new SplashScreenManager(loadingfrm);
                loading.ShowLoading();
                Display_Point();
                loading.CloseWaitForm();
            }
        }

        //切换为显示线段
        private void lines_check_CheckedChanged(object sender, EventArgs e)
        {
            if (lines_check.Checked == true)
            {
                Loading loadingfrm = new Loading(this);
                SplashScreenManager loading = new SplashScreenManager(loadingfrm);
                loading.ShowLoading();
                Display_Line();
                loading.CloseWaitForm();
            }
        }

        //切换为显示三角形
        private void tris_check_CheckedChanged(object sender, EventArgs e)
        {
            if (tris_check.Checked == true)
            {
                Loading loadingfrm = new Loading(this);
                SplashScreenManager loading = new SplashScreenManager(loadingfrm);
                loading.ShowLoading();
                Display_Tri();
                loading.CloseWaitForm();
            }
        }

        private void DisplayTopology_Load(object sender, EventArgs e)
        {
            Loading loadingfrm = new Loading(this);
            SplashScreenManager loading = new SplashScreenManager(loadingfrm);
            loading.ShowLoading();
            Display_Point();
            loading.CloseWaitForm();
        }
    }
}
