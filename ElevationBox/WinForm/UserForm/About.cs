﻿using System;
using System.Windows.Forms;

namespace WinForm
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitee.com/hunter1024/ElevationBox");
        }
    }
}
