﻿namespace WinForm
{
    partial class DisplayTopology
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DisplayTopology));
            this.points_check = new System.Windows.Forms.RadioButton();
            this.lines_check = new System.Windows.Forms.RadioButton();
            this.tris_check = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // points_check
            // 
            this.points_check.AutoSize = true;
            this.points_check.Checked = true;
            this.points_check.Location = new System.Drawing.Point(127, 16);
            this.points_check.Name = "points_check";
            this.points_check.Size = new System.Drawing.Size(59, 16);
            this.points_check.TabIndex = 0;
            this.points_check.TabStop = true;
            this.points_check.Text = "高程点";
            this.points_check.UseVisualStyleBackColor = true;
            this.points_check.CheckedChanged += new System.EventHandler(this.points_check_CheckedChanged);
            // 
            // lines_check
            // 
            this.lines_check.AutoSize = true;
            this.lines_check.Location = new System.Drawing.Point(269, 16);
            this.lines_check.Name = "lines_check";
            this.lines_check.Size = new System.Drawing.Size(47, 16);
            this.lines_check.TabIndex = 1;
            this.lines_check.Text = "线段";
            this.lines_check.UseVisualStyleBackColor = true;
            this.lines_check.CheckedChanged += new System.EventHandler(this.lines_check_CheckedChanged);
            // 
            // tris_check
            // 
            this.tris_check.AutoSize = true;
            this.tris_check.Location = new System.Drawing.Point(399, 16);
            this.tris_check.Name = "tris_check";
            this.tris_check.Size = new System.Drawing.Size(59, 16);
            this.tris_check.TabIndex = 2;
            this.tris_check.Text = "三角形";
            this.tris_check.UseVisualStyleBackColor = true;
            this.tris_check.CheckedChanged += new System.EventHandler(this.tris_check_CheckedChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(560, 290);
            this.dataGridView1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(365, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "说明：三角形邻接关系中的-1代表该边无临界三角形，即为边界边。";
            // 
            // DisplayTopology
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 372);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tris_check);
            this.Controls.Add(this.lines_check);
            this.Controls.Add(this.points_check);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DisplayTopology";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "显示拓扑数据";
            this.Load += new System.EventHandler(this.DisplayTopology_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton points_check;
        private System.Windows.Forms.RadioButton lines_check;
        private System.Windows.Forms.RadioButton tris_check;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
    }
}