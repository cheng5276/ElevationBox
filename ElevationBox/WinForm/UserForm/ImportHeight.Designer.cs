﻿namespace WinForm
{
    partial class ImportHeight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportHeight));
            this.calc = new System.Windows.Forms.Button();
            this.cleardata = new System.Windows.Forms.Button();
            this.data = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.unit1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.unit2 = new System.Windows.Forms.Label();
            this.fill = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.unit3 = new System.Windows.Forms.Label();
            this.cut = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // calc
            // 
            this.calc.Location = new System.Drawing.Point(154, 198);
            this.calc.Name = "calc";
            this.calc.Size = new System.Drawing.Size(75, 30);
            this.calc.TabIndex = 10;
            this.calc.Text = "计算";
            this.calc.UseVisualStyleBackColor = true;
            this.calc.Click += new System.EventHandler(this.calc_Click);
            // 
            // cleardata
            // 
            this.cleardata.Location = new System.Drawing.Point(56, 198);
            this.cleardata.Name = "cleardata";
            this.cleardata.Size = new System.Drawing.Size(75, 30);
            this.cleardata.TabIndex = 9;
            this.cleardata.Text = "清空";
            this.cleardata.UseVisualStyleBackColor = true;
            this.cleardata.Click += new System.EventHandler(this.cleardata_Click);
            // 
            // data
            // 
            this.data.Location = new System.Drawing.Point(63, 27);
            this.data.Name = "data";
            this.data.Size = new System.Drawing.Size(138, 21);
            this.data.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "高  程：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.unit1);
            this.groupBox1.Controls.Add(this.data);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(259, 64);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "输入平整面高程";
            // 
            // unit1
            // 
            this.unit1.AutoSize = true;
            this.unit1.Location = new System.Drawing.Point(206, 31);
            this.unit1.Name = "unit1";
            this.unit1.Size = new System.Drawing.Size(11, 12);
            this.unit1.TabIndex = 9;
            this.unit1.Text = " ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cut);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.unit2);
            this.groupBox2.Controls.Add(this.fill);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.unit3);
            this.groupBox2.Location = new System.Drawing.Point(13, 84);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 103);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "计算结果";
            // 
            // unit2
            // 
            this.unit2.AutoSize = true;
            this.unit2.Location = new System.Drawing.Point(196, 31);
            this.unit2.Name = "unit2";
            this.unit2.Size = new System.Drawing.Size(11, 12);
            this.unit2.TabIndex = 12;
            this.unit2.Text = " ";
            // 
            // fill
            // 
            this.fill.Location = new System.Drawing.Point(63, 27);
            this.fill.Name = "fill";
            this.fill.Size = new System.Drawing.Size(130, 21);
            this.fill.TabIndex = 11;
            this.fill.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fill_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "填方量：";
            // 
            // unit3
            // 
            this.unit3.AutoSize = true;
            this.unit3.Location = new System.Drawing.Point(196, 66);
            this.unit3.Name = "unit3";
            this.unit3.Size = new System.Drawing.Size(11, 12);
            this.unit3.TabIndex = 15;
            this.unit3.Text = " ";
            // 
            // cut
            // 
            this.cut.Location = new System.Drawing.Point(63, 61);
            this.cut.Name = "cut";
            this.cut.Size = new System.Drawing.Size(130, 21);
            this.cut.TabIndex = 14;
            this.cut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cut_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "挖方量：";
            // 
            // ImportHeight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 241);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.calc);
            this.Controls.Add(this.cleardata);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportHeight";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "计算土方量";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button calc;
        private System.Windows.Forms.Button cleardata;
        public System.Windows.Forms.TextBox data;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox cut;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox fill;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label unit1;
        public System.Windows.Forms.Label unit2;
        public System.Windows.Forms.Label unit3;
    }
}