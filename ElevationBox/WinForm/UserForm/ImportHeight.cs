﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinForm
{
    public partial class ImportHeight : Form
    {
        private TIN tin;
        public ImportHeight(TIN tin)
        {
            InitializeComponent();
            this.tin = tin;
        }

        private void cleardata_Click(object sender, EventArgs e)
        {
            data.Text = fill.Text = cut.Text = "";
        }

        private void calc_Click(object sender, EventArgs e)
        {
            if (data.Text == "")
            {
                MessageBox.Show("请输入平整面高程！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                double height = Convert.ToDouble(data.Text);
                if (height > 8000 || height < -100)
                {
                    MessageBox.Show("请输入一个在[-100,8000]之间的高程！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                double[] value = tin.create_earthwork(height);
                fill.Text = Math.Abs(value[1]).ToString("0.000");
                cut.Text = value[0].ToString("0.000");
            }
            catch
            {
                MessageBox.Show("输入的数据有误，请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        //禁止向填方量文本框输入内容
        private void fill_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        //禁止向挖方量文本框输入内容
        private void cut_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
