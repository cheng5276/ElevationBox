﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WinForm
{
    public partial class ImportPoint : Form
    {
        public ImportPoint()
        {
            InitializeComponent();
        }

        /*--选择文件--*/
        private void choosefile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "选择文件";
            ofd.Filter = "文本文档|*.txt";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filepath.Text = ofd.FileName;
            }
            else
            {
                return;
            }   
        }

        /*--自动导入--*/
        private void getpoint1_Click(object sender, EventArgs e)
        {
            if (filepath.Text == "")
            {
                MessageBox.Show("请选择数据文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (File.Exists(filepath.Text))
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("文件不存在，请重新选择数据文件", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 
        }

        /*--手动输入--*/
        private void getpoint2_Click(object sender, EventArgs e)
        {
            if (stringdata.Text == "")
            {
                MessageBox.Show("请输入高程点数据！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }            
            this.DialogResult = DialogResult.Yes;
        }

        /*--清空输入--*/
        private void cleardata_Click(object sender, EventArgs e)
        {
            stringdata.Text = "";
        }
    }
}
