﻿using System;

namespace WinForm
{
    public class TIN_Tri
    {
        //三条线段在基线数据集的下标
        public int L0 { get; set; }
        public int L1 { get; set; }
        public int L2 { get; set; }
        //邻接三角形在三角形数据集的下标
        public int[] Adjoin = { -1, -1, -1 };
        //是否为边界三角形
        public bool Edge { get; set; }
    }
}
