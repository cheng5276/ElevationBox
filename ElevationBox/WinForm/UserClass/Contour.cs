﻿using System;
using System.Collections.Generic;

namespace WinForm
{
    class Contour
    {
        public double h { get; set; }//存储等值线的高程
        public List<TIN_Point> points = new List<TIN_Point>();//存储等值线上的所有点
    }
}
