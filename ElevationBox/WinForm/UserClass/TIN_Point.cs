﻿using System;

namespace WinForm
{   
    public class TIN_Point
    {        
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}
