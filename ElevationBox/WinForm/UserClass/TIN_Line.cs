﻿using System;

namespace WinForm
{
    public class TIN_Line
    {
        //两端点在离散点集的下标
        public int P0 { get; set; }
        public int P1 { get; set; }
        //线段的使用次数
        public int Count { get; set; }
    }
}
