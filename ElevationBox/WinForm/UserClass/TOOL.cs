﻿using System;

namespace WinForm
{
    public class TOOL
    {
        //计算两点之间的平距
        public static double Distance(TIN_Point A, TIN_Point B)
        {
            double deltaX = B.X - A.X;
            double deltaY = B.Y - A.Y;
            double temp = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
            return temp;
        }
        
        //计算点C处的角度，单位弧度
        public static double Angle(TIN_Point A, TIN_Point B, TIN_Point C)
        {
            double c = Distance(A, B);
            double a = Distance(A, C);
            double b = Distance(B, C);
            double temp = Math.Acos((a * a + b * b - c * c) / (2 * a * b));
            return temp;
        }

        //计算点C在竖直方向到点A\点B所连直线的距离
        public static double Sign(TIN_Point A,TIN_Point B, TIN_Point C)
        {
            double a = (B.Y - A.Y) / (B.X - A.X);
            double b = (B.X * A.Y - A.X * B.Y) / (B.X - A.X);
            double temp = C.Y - a * C.X - b;
            return temp;
        }

        //判断两条线段是否为同一条
        public static bool Equaltion(TIN_Line L1, TIN_Line L2)
        {
            if (L1.P0 == L2.P0 && L1.P1 == L2.P1)
            {
                return true;
            }
            if (L1.P0 == L2.P1 && L1.P1 == L2.P0)
            {
                return true;
            }
            return false;
        }

        //判断两个相邻三角形的临边在第一个三角形的哪个位置
        public static int Coincide(TIN_Tri T1, TIN_Tri T2)
        {
            int[] m = { T1.L0, T1.L1, T1.L2 };
            int[] n = { T2.L0, T2.L1, T2.L2 };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (m[i] == n[j])
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        //判断两个三角形是否存在邻接关系
        public static bool Equaltion2(TIN_Tri T1, TIN_Tri T2)
        {
            int[] m = { T1.L0, T1.L1, T1.L2 };
            int[] n = { T2.L0, T2.L1, T2.L2 };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (m[i] == n[j])
                    {
                        return true;
                    }
                } 
            }
            return false;
        }

        //计算TIN插值点坐标
        public static TIN_Point CalIsoVal(TIN_Point a, TIN_Point b, double h)
        {
            TIN_Point temp = new TIN_Point();
            double val = (h - a.Z) / (b.Z - a.Z);
            temp.X = a.X + (b.X - a.X) * val;
            temp.Y = a.Y + (b.Y - a.Y) * val;
            temp.Z = h;
            return temp;
        }

        //判断线段上是否存在高度为h的等高线点
        public static bool Existence(double z1, double z2, double h)
        {
            if ((z2 - h) == 0)
            {
                return true;
            }
            if ((z1 - h) / (z2 - h) <= 0)
            {
                return true;
            }
            else return false;
        }

        //判断三角行的边界边的下标
        public static int EdgeLine(TIN_Tri A)
        {
            int index = Array.IndexOf(A.Adjoin, -1);
            int edegline;
            if (index == 0)
            {
                edegline = A.L0;
            }
            else if (index == 1)
            {
                edegline = A.L1;
            }
            else
            {
                edegline = A.L2;
            }
            return edegline;
        }

        //判断三角形三个顶点高程值与设计高关系
        public static int Relation(double h1, double h2, double h3, double h0)
        {
            if (h0 <= h1 && h0 <= h2 && h0 <= h3)
            {
                return 1;
            }
            else if (h0 >= h1 && h0 >= h2 && h0 >= h3)
            {
                return -1;
            }
            else return 0;
        }

        //判断三角形没有等值点的那条边与设计高关系
        public static int Relation2(double h1, double h2, double h0)
        {
            if (h0 <= h1 && h0 <= h2)
            {
                return 1;
            }
            else return -1;
        }

        //计算三角形水平投影面积
        public static double Area(TIN_Point A,TIN_Point B,TIN_Point C)
        {
            double c = Distance(A, B);
            double a = Distance(A, C);
            double b = Distance(B, C);
            double p = (a + b + c) / 2;
            double area = Math.Sqrt(p * (p - a) * (p - b) * (p - c));
            return area;
        }
    }
}
